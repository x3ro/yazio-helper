export const createLocalStorageState = <T>(name: string, initial: T) => {
    let value = $state(JSON.parse(localStorage.getItem(name) ?? 'null') ?? initial)

    return {
        get value() {
            return value
        },
        set value(val) {
            localStorage.setItem(name, JSON.stringify(val))
            value = val
        },
    }
}
