import { createLocalStorageState } from './localStorageState.svelte'

export const foods = createLocalStorageState('foods', '')
export const eaten = createLocalStorageState('eaten', 0)
