# Yazio Helper

This is a simple app to help me with adding meals to my Yazio. Since I usually
cook for my family, I can't just add the amount of ingredients I used to the
Yazio app to track my calories. Often times, I have to note down the amount of
each ingredient I used and then calculate the share of the meal I ate by waying
what I took from the pot.

This app is supposed to help me with that. I can add the ingredients I used and
the amount of the meal I ate and it will calculate the amount of each ingredient
I ate, so I can add it to the Yazio app.


## Usage

The app is pretty simple. You can add ingredients line by line and specify the
amount of the meal you ate. The app will then calculate the amount of each
ingredient you ate and display it in a table.

Each line starts with the amount of the ingredient in grams, followed by the
name of the ingredient. The amount of the meal you ate is specified in the
bottom input field.

Example:

> ```text
> 200 apple
> 100 banana
> 50 orange
> ```
>
> Eaten: `180` g

The above means that you used 200g of apple, 100g of banana and 50g of orange
to make a fruit salad. You then ate 180g of the fruit salad and generously as
you are, you left the rest for your family.

The app will then calculate the amount of each ingredient you ate and display it
in a table:

```text
102.86 g  apple
 51.43 g  banana
 25.71 g  orange
```


## Running the App

Clone the repository and install npm dependencies. Then run it locally:

```console
$ git clone https://gitlab.com/x3ro/yazio-helper
$ cd yazio-helper
$ npm install
$npm run dev -- --open
```


## Building

To create a production version of the app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
